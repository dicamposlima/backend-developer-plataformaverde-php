FROM php:7.4-fpm-alpine

RUN apk add --no-cache bash libpng libpng-dev && docker-php-ext-install gd && apk del libpng-dev

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- \
    --filename=composer \
    --install-dir=/usr/local/bin
