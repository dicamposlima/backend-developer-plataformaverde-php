<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::get('residuos/status', 'ResiduosController@status');
    Route::get('residuos', 'ResiduosController@index');
    Route::post('residuos', 'ResiduosController@store');
    Route::get('residuos/{id}', 'ResiduosController@show');
    Route::put('residuos/{id}', 'ResiduosController@update');
    Route::delete('residuos/{id}', 'ResiduosController@destroy');
});
