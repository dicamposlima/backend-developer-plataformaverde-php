<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Residuos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:255',
            'tipo' => 'required|max:255',
            'categoria' => 'required|max:255',
            'tecnologia_tratamento' => 'required|max:255',
            'classe' => 'required|max:255',
            'unidade_medida' => 'required|max:255',
            'peso' => 'required',
        ];
    }
}
