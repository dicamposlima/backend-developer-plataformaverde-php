<?php

namespace App\Http\Resources;

use App\Residuos;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ResiduosCollection extends ResourceCollection
{

    public static $wrap = 'residuos';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Residuos $residuo) {
            return (new ResiduosResource($residuo));
        });

        return [
            "status" => 200,
            'residuos' => $this->collection
        ];
    }
}
