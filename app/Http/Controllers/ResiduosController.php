<?php

namespace App\Http\Controllers;

use App\Http\Requests\Residuos as RequestsResiduos;
use App\Residuos;
use Illuminate\Http\Request;
use App\Imports\ResiduosImport;
use App\Exceptions\EmptyRowException;
use App\Http\Resources\ResiduosCollection;
use App\Http\Resources\ResiduosResource;
use Maatwebsite\Excel\Facades\Excel;

class ResiduosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\ResourceCollection
     */
    public function index(Request $request)
    {

        $residuos = new ResiduosCollection(Residuos::paginate(25));

        if (count($residuos) <= 0) {
            return response()->json([
                "status" => 404,
                "message" => "Não encontrado",
                "description" => "Residuos não encontrado",
            ], 404);
        }

        return $residuos;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status()
    {
        if (Residuos::hasFailedJobs()) {
            return response()->json([
                "status" => 200,
                "message" => "Processamento falhou",
                "description" => "O processamento da planilha falhou",
            ], 200);
        }

        if (Residuos::hasActiveJobs()) {
            return response()->json([
                "status" => 200,
                "message" => "Processando arquivo",
                "description" => "Os dados estao sendo processsados",
            ], 200);
        }

        $description = "O arquivo foi processado e os dados foram salvos.";
        $description .= " Para visualizar o status acesse /api/v1/residuos";
        return response()->json([
            "status" => 200,
            "message" => "Arquivo processado",
            "description" => $description,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if ($request->hasFile('residuos_excel')) {
            try {
                Excel::queueImport(new ResiduosImport, $request->file('residuos_excel'));
                return response()->json([
                    "message" => "Processando arquivo",
                    "description" => "Para visualizar o status acesse /api/v1/residuos/status",
                ], 201);
            } catch (EmptyRowException $e) {
                return response()->json([
                    "status" => 400,
                    "message" => $e->getMessage(),
                    "description" => " Remova todas a linhas e colunas em branco",
                ], 400);
            }
        }
        return response()->json([
            "status" => 400,
            "message" => "Erro no payload",
            "description" => "Arquivo não encontrado, verifique se o nome da key é 'residuos_excel'",
        ], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        if (Residuos::where('id', $id)->exists()) {
            $residuo = new ResiduosResource(Residuos::find($id));
            return response()->json([
                "status" => 200,
                "residuo" => $residuo
            ], 200);
        }

        return response()->json([
            "status" => 404,
            "message" => "Não encontrado",
            "description" => "Residuo não encontrado",
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Residuos  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RequestsResiduos $request, $id)
    {
        if (Residuos::where('id', $id)->exists()) {
            $residuo = Residuos::find($id);
            $residuo->nome = is_null($request->nome)
                ? $residuo->nome : $request->nome;
            $residuo->tipo = is_null($request->tipo)
                ? $residuo->tipo : $request->tipo;
            $residuo->categoria = is_null($request->categoria)
                ? $residuo->categoria : $request->categoria;
            $residuo->tecnologia_tratamento = is_null($request->tecnologia_tratamento)
                ? $residuo->tecnologia_tratamento : $request->tecnologia_tratamento;
            $residuo->classe = is_null($request->classe)
                ? $residuo->classe : $request->classe;
            $residuo->unidade_medida = is_null($request->unidade_medida)
                ? $residuo->unidade_medida : $request->unidade_medida;
            $residuo->peso = is_null($request->peso)
                ? $residuo->peso : floatval($request->peso);

            $residuo->save();

            return response()->json([
                "status" => 201,
                "message" => "Residuo modificado",
                "description" => "O Residuo com o id:{$id} foi modificado"
            ], 201);
        }

        return response()->json([
            "status" => 404,
            "message" => "Residuo não encontrado",
            "description" => "O Residuo com o id:{$id} não foi encontrado"
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (Residuos::where('id', $id)->exists()) {
            $residuo = Residuos::find($id);
            $residuo->delete();

            return response()->json([
                "status" => 201,
                "message" => "Residuo excluído",
                "description" => "O Residuo com o id:{$id} foi excluído"
            ], 201);
        }
        return response()->json([
            "status" => 404,
            "message" => "Residuo não encontrado",
            "description" => "O Residuo com o id:{$id} não foi encontrado"
        ], 404);
    }
}
