<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Residuos extends Model
{
    protected $table = 'residuos';

    protected $fillable = ['nome', 'tipo', 'categoria', 'tecnologia_tratamento', 'classe', 'unidade_medida', 'peso'];


    /**
     * Check if exists any failed job
     *
     * @return bool
     */
    public static function hasFailedJobs(): bool
    {
        $failed_jobs = DB::table('failed_jobs')
            ->where('queue', 'default')
            ->orderByDesc('id')
            ->first();
        return $failed_jobs ? true : false;
    }

    /**
     * Check if exists any job processing
     *
     * @return bool
     */
    public static function hasActiveJobs(): bool
    {
        $jobs = DB::table('jobs')
            ->where('queue', 'default')
            ->orderByDesc('id')
            ->first();
        return $jobs ? true : false;
    }
}
