<?php

namespace App\Imports;

use App\Residuos;
use App\Exceptions\EmptyRowException;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResiduosImport implements OnEachRow, WithChunkReading, ShouldQueue
{

    /**
     *
     *
     * @return \Maatwebsite\Excel\Row
     * @return void
     */
    public function onRow(Row $row): void
    {
        $rowIndex = $row->getIndex();
        if ($rowIndex <= 1) { // Pula o cabecalho
            return;
        }

        $row = $row->toArray();

        if ($row[0] === null) {
            throw new EmptyRowException("Arquivo mal formatado");
        }

        $residuo = new Residuos;

        $residuo->nome = trim($row[0]);
        $residuo->tipo = trim($row[1]);
        $residuo->categoria = trim($row[2]);
        $residuo->tecnologia_tratamento = trim($row[3]);
        $residuo->classe = trim($row[4]);
        $residuo->unidade_medida = trim($row[5]);
        $residuo->peso = floatval(trim($row[6]));
        $residuo->save();
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
