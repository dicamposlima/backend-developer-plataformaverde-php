# Plataforma Verde - Backend Developer Test Api Documentation
## LOCAL ENVIRONMENT


## Instale a última versão do [PHP](https://www.php.net/downloads.php), do [MySql](https://dev.mysql.com/downloads) e do [Composer](https://getcomposer.org/download) e siga os passos abaixo

### `Passo 1`
Crie o esquema de banco de dados executando a query abaixo:
```bash
CREATE SCHEMA `plataforma_verde_backend` DEFAULT CHARACTER SET utf8 ;
```

### `Passo 2`
Altere a configuração do banco de dados no arquivo **_`.env`_**

Ex.:
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=plataforma_verde_backend
DB_USERNAME=laravel
DB_PASSWORD=123456
```

### `Passo 3`
Adicione as extensoes no **_`php.ini`_**
```bash
extension=php_fileinfo.dll
extension=php_gd2.dll
```

### `Passo 4`
Pelo terminal acesse a página no projeto e execute o comando abaixo:

4.1 Para installar as dependências:
```bash
composer update
```
4.2 Para criar as tabelas no banco de dados
```bash
php artisan migrate
```

4.3 Para fazer rollback das migrações execute o comando abaixo:
```bash
php artisan migrate:rollback
```

### `Passo 5`
Pelo terminal execute o comando abaixo se quiser vê-lo rodando no navegador.
```bash
php artisan serve
```

5.1 Acesse a url [http://127.0.0.1:8000] para ver a documentacao da api caso precise.

### `Passo 6`
Para executar os testes execute o comando abaixo direto no terminal.
```bash
./vendor/bin/phpunit
```

6.1 Para ver o test coverage abra o arquivo index.html no navegador, o arquivo está localizado na pasta: **_`/tests/build/coverage/index.html`_**

Obs. é necessário habilitar o [xdebug](https://xdebug.org) para o test coverage.

## License

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
