<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Residuos;

class ResiduosTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function it_not_found_data()
    {
        $response = $this->get('/api/v1/residuos');
        $response->assertNotFound();
    }

    /**
     * @test
     */
    public function it_can_access_list()
    {
        factory(Residuos::class)->create();
        $response = $this->get('/api/v1/residuos');
        $response->assertOk();
    }

    /**
     * @test
     */
    public function it_list_not_empty()
    {
        factory(Residuos::class)->create();
        $response = $this->get('/api/v1/residuos');
        $response->assertOk()
            ->assertJsonCount(1, 'residuos');
    }

    /**
     * @test
     */
    public function it_list_has_exact_number()
    {
        factory(Residuos::class)->create();
        factory(Residuos::class)->create();
        factory(Residuos::class)->create();
        $response = $this->get('/api/v1/residuos');
        $response->assertOk()
            ->assertJsonCount(3, 'residuos');
    }

    /**
     * @test
     */
    public function it_can_update_data()
    {
        $residuo = factory(Residuos::class)->create();
        $response = $this->put("/api/v1/residuos/{$residuo->id}", [
            'nome' => "Alumínio",
            'tipo' => "Metal Não Ferroso",
            'categoria' => "Não Reciclável",
            'tecnologia_tratamento' => "Reciclagem",
            'classe' => "||-B",
            'unidade_medida' => "kg",
            'peso' => "100.5",
        ]);
        $response->assertStatus(201);
    }

    /**
     * @test
     */
    public function it_cannot_update_data()
    {
        $residuo = factory(Residuos::class)->create();
        $response = $this->put("/api/v1/residuos/{$residuo->id}", [
            'tipo' => "Metal Não Ferroso",
            'categoria' => "Não Reciclável",
            'tecnologia_tratamento' => "Reciclagem",
            'classe' => "||-B",
            'unidade_medida' => "kg",
            'peso' => "100.5",
        ]);
        $response->assertStatus(422);


        $residuo = factory(Residuos::class)->create();
        $response = $this->put("/api/v1/residuos/10", [
            'nome' => "Alumínio",
            'tipo' => "Metal Não Ferroso",
            'categoria' => "Não Reciclável",
            'tecnologia_tratamento' => "Reciclagem",
            'classe' => "||-B",
            'unidade_medida' => "kg",
            'peso' => "100.5",
        ]);
        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function it_can_view_data()
    {
        $residuo = factory(Residuos::class)->create([
            "nome" => "Copos Plásticos Contaminado"
        ]);
        $response = $this->get("/api/v1/residuos/{$residuo->id}");
        $response->assertOk()
            ->assertJsonPath('residuo.nome', $residuo->nome);
    }

    /**
     * @test
     */
    public function it_cannot_view_data()
    {
        factory(Residuos::class)->create();
        $response = $this->get("/api/v1/residuos/10");
        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function it_can_delete_data()
    {
        $residuo = factory(Residuos::class)->create([
            "nome" => "Copos Plásticos Contaminado"
        ]);
        $response = $this->delete("/api/v1/residuos/{$residuo->id}");
        $response->assertStatus(201);

        $response = $this->get("/api/v1/residuos/{$residuo->id}");
        $response->assertNotFound();
    }

    /**
     * @test
     */
    public function it_cannot_delete_data()
    {
        factory(Residuos::class)->create();
        $response = $this->delete("/api/v1/residuos/10");
        $response->assertStatus(404);
    }
}
