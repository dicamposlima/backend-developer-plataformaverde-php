<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResiduosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residuos', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('nome', 255)->nullable(false);
            $table->string('tipo', 255)->nullable(false);
            $table->string('categoria', 255)->nullable(false);
            $table->string('tecnologia_tratamento', 255)->nullable(false);
            $table->string('classe', 255)->nullable(false);
            $table->string('unidade_medida', 255)->nullable(false);
            $table->decimal('peso', 9, 3)->nullable(false)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residuos');
    }
}
