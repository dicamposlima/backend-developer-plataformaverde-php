<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Residuos;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Residuos::class, function (Faker $faker) {
    return [
        'nome' => "Papelão",
        'tipo' => "Construção Civil",
        'categoria' => "Reciclàvel",
        'tecnologia_tratamento' => "Aterro Comum",
        'classe' => "||-A",
        'unidade_medida' => "ton",
        'peso' => "525.134",
    ];
});
